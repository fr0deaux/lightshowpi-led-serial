#!/usr/bin/env python
#
# Licensed under the BSD license.  See full license in LICENSE file.
# http://www.lightshowpi.com/
#
# Author: Tom Enos (tomslick.ca@gmail.com)


"""wrapper module for Bibliopixel for use with lightshowpi

"""
import importlib
import math
from bibliopixel.led import *
from bibliopixel.drivers.driver_base import *
from bibliopixel.drivers.serial_driver import *
import bibliopixel.colors as colors
import bibliopixel.image as image
from bibliopixel import log
import numpy
from PIL import Image, ImageSequence, ImageChops, ImageEnhance


log.setLogLevel(log.WARNING)
#log.setLogLevel(log.DEBUG)

color_map = [colors.hue2rgb(c) for c in range(256)]
rainbow_map = [colors.hue2rgb_rainbow(c) for c in range(256)]
scale = colors.color_scale
int_map=[colors.Green for i in range(0,80)] + [colors.Yellow for i in range(80,160)] + [colors.Red for i in range(160,256)]


class LedSPI(object):
    def __init__(self, driver_type, led_channels, led_per_channel, color_order, max_brightness, led_update_throttle, p_color_map, p_color, p_type):
        led_number = led_channels * led_per_channel
        self.last = led_channels - 1
        self.lpc = led_per_channel
        self.skip = led_update_throttle 
        self.update_skip = self.skip 
	self.max_b = max_brightness / 100.0
        channel_order = getattr(ChannelOrder, color_order)
        main_driver = importlib.import_module("bibliopixel.drivers." + driver_type)
        my_driver = getattr(main_driver, "Driver" + driver_type)
        self.driver = my_driver(num=led_number,
                                     c_order=channel_order,
                                     use_py_spi=True)
        
        self.led = LEDStrip(self.driver)
#        self.led = LEDStrip(self.driver, threadedUpdate = True)
	self.led.setMasterBrightness(int(self.max_b * 255))

        self.pcolor = p_color
        self.pmap = p_color_map
        self.ptype = p_type
    

    def writefill(self,pinlist):

        if self.update_skip <> 0:
            self.update_skip = self.update_skip - 1
            if self.update_skip >= 0:
                return

        self.led.all_off()

        for pin in xrange(len(pinlist)):

            level = pinlist[pin]
            brightness = int(level * 255)
            sled = pin * self.lpc

            if self.pmap == 'MONO': 
                rgb = (int(level * self.pcolor[0]) ,int(level * self.pcolor[1]) ,int(level * self.pcolor[2]))
            elif self.pmap == 'FREQ1':
                rgb = color_map[int((float(pin)/(self.last+1)) * 255)]
                rgb = (int(rgb[0] * level), int(rgb[1] * level), int(rgb[2] * level))
            elif self.pmap == 'MAP1':
                rgb = scale(color_map[brightness],brightness)
            elif self.pmap == 'MAP2':
                rgb = scale(color_map[255 - brightness],brightness)
            else:
                rgb = (brightness,brightness,brightness)

            if self.ptype == 'CBARS':
                midl = int(self.lpc / 2)
                mlvl = int ( level * midl )
                self.led.fill(rgb, sled + midl - mlvl , sled + midl + mlvl )
            elif self.ptype == 'FULL':
                self.led.fill(rgb, sled, sled + self.lpc - 1)
            elif self.ptype == 'LBARS':
                midl = int(self.lpc / 2) + sled
                for gled in range(0,int((self.lpc/2) * level)):
                    self.led.set(midl + gled,int_map[int((float(gled)/(self.lpc/2))*255)])
                    self.led.set(midl - gled,int_map[int((float(gled)/(self.lpc/2))*255)])


        self.led.update()
        self.update_skip = self.skip 


class LedSerial(object):
    def __init__(self, driver_type, device_id, led_channels, led_per_channel, color_order, max_brightness, led_update_throttle, p_color_map, p_color, p_type):
        led_number = led_channels * led_per_channel
        self.last = led_channels - 1
        self.lpc = led_per_channel
        self.skip = led_update_throttle 
        self.update_skip = self.skip 
	self.max_b = max_brightness / 100.0
        channel_order = getattr(ChannelOrder, color_order)
	driver_type = getattr(LEDTYPE, driver_type) 
        self.driver = DriverSerial(num=led_number,
                                   c_order=channel_order,
                                   type=driver_type,
                                   deviceID=device_id)
        
        self.led = LEDStrip(self.driver)
#        self.led = LEDStrip(self.driver, threadedUpdate = True)
	self.led.setMasterBrightness(int(self.max_b * 255))

        self.pcolor = p_color
        self.pmap = p_color_map
        self.ptype = p_type
        

    def writefill(self,pinlist):

        if self.update_skip <> 0:
            self.update_skip = self.update_skip - 1
            if self.update_skip >= 0:
                return

        self.led.all_off()

        for pin in xrange(len(pinlist)):

            level = pinlist[pin]
            brightness = int(level * 255)
            sled = pin * self.lpc

            if self.pmap == 'MONO': 
                rgb = (int(level * self.pcolor[0]) ,int(level * self.pcolor[1]) ,int(level * self.pcolor[2]))
            elif self.pmap == 'FREQ1':
                rgb = color_map[int((float(pin)/(self.last+1)) * 255)]
                rgb = (int(rgb[0] * level), int(rgb[1] * level), int(rgb[2] * level))
            elif self.pmap == 'MAP1':
                rgb = scale(color_map[brightness],brightness)
            elif self.pmap == 'MAP2':
                rgb = scale(color_map[255 - brightness],brightness)
            else:
                rgb = (brightness,brightness,brightness)

            if self.ptype == 'CBARS':
                midl = int(self.lpc / 2)
                mlvl = int ( level * midl )
                self.led.fill(rgb, sled + midl - mlvl , sled + midl + mlvl )
            elif self.ptype == 'FULL':
                self.led.fill(rgb, sled, sled + self.lpc - 1)
            elif self.ptype == 'LBARS':
                midl = int(self.lpc / 2) + sled
                for gled in range(0,int((self.lpc/2) * level)):
                    self.led.set(midl + gled,int_map[int((float(gled)/(self.lpc/2))*255)])
                    self.led.set(midl - gled,int_map[int((float(gled)/(self.lpc/2))*255)])


        self.led.update()
        self.update_skip = self.skip 

        
class LedSerialMatrix(object):
    def __init__(self, driver_type, device_id, led_channels, led_width, led_height, color_order, max_brightness, led_update_throttle, p_type):
        self.skip = led_update_throttle
        self.update_skip = self.skip 
        self.last = led_channels - 1
	self.max_b = max_brightness / 100.0
        self.images = []
        self.ptype = p_type
        channel_order = getattr(ChannelOrder, color_order)
	driver_type = getattr(LEDTYPE, driver_type) 
        led_number = led_width * led_height
        self.driver = DriverSerial(num=led_number,
                                   c_order=channel_order,
                                   type=driver_type,
                                   deviceID=device_id)
        
        self.led = LEDMatrix(self.driver,
                                   width=led_width,
                                   height=led_height,
                                   serpentine=True,
                                   vert_flip=True,
                                   threadedUpdate=False)
	self.led.setMasterBrightness(int(self.max_b * 255))

        imagepath = os.getenv("SYNCHRONIZED_LIGHTS_HOME") + "/16xstar.gif" 
        for frame in ImageSequence.Iterator(Image.open(imagepath)):
            rgba = Image.new("RGBA", frame.size)
            rgba.paste(frame)
            self.images.append(rgba)

        self.drops = [[0 for x in range(led_channels)] for y in range(led_height)]
        self.height = led_height
        self.width = led_width

    def writefill(self,pinlist):

        if self.update_skip <> 0:
            self.update_skip = self.update_skip - 1
            if self.update_skip >= 0:
                return

        self.led.all_off()

        if self.ptype == 'MBARS':
            normarr = [ int(x * 255) for x in pinlist ]
            self.drops.append(normarr)
            for y in range(self.height): 
                for x in range(self.width): 
                    xind = int(((self.last + 1.0) / self.width) * x)
                    if (self.drops[y][xind] > 64):
                        rgb = scale(color_map[255 - self.drops[y][xind] ],int(self.drops[y][xind] * 0.5))
                        self.led.set(x,y,rgb)
            del self.drops[0]

        elif self.ptype == 'IMAGE':

            completeimage = Image.new("RGBA", self.images[0].size)

            for pin in xrange(len(pinlist)):
                if (pinlist[pin] > 0.25):
                    completeimage = ImageChops.add_modulo(completeimage,ImageEnhance.Brightness(self.images[pin]).enhance(pinlist[pin]))

            image.showImage(self.led, "", ImageEnhance.Brightness(completeimage).enhance(self.max_b * 0.5))


        self.led.update()
        self.update_skip = self.skip 

